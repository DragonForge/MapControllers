package com.zeitheron.mapcontrollers.commands;

import java.util.ArrayList;
import java.util.List;

import com.zeitheron.hammercore.HammerCore;
import com.zeitheron.hammercore.utils.WorldUtil;
import com.zeitheron.mapcontrollers.api.ISpeakerController;

import net.minecraft.command.CommandBase;
import net.minecraft.command.CommandException;
import net.minecraft.command.ICommandSender;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.server.MinecraftServer;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;

public class CommandSpeakers extends CommandBase
{
	@Override
	public void execute(MinecraftServer server, ICommandSender sender, String[] args) throws CommandException
	{
		if(args[0].equalsIgnoreCase("start"))
		{
			BlockPos pos = CommandBase.parseBlockPos(sender, args, 1, false);
			World world = sender.getEntityWorld();
			if(!world.isBlockLoaded(pos))
				return;
			ISpeakerController c = WorldUtil.cast(world.getTileEntity(pos), ISpeakerController.class);
			if(c == null)
				throw new CommandException("Speaker Controller not found!", new Object[0]);
			c.start();
			return;
		} else if(args[0].equalsIgnoreCase("stop"))
		{
			BlockPos pos = CommandBase.parseBlockPos(sender, args, 1, false);
			World world = sender.getEntityWorld();
			if(!world.isBlockLoaded(pos))
				return;
			ISpeakerController c = WorldUtil.cast(world.getTileEntity(pos), ISpeakerController.class);
			if(c == null)
				throw new CommandException("Speaker Controller not found!", new Object[0]);
			c.stop();
			return;
		} else if(args[0].equalsIgnoreCase("pause"))
		{
			BlockPos pos = CommandBase.parseBlockPos(sender, args, 1, false);
			World world = sender.getEntityWorld();
			if(!world.isBlockLoaded(pos))
				return;
			ISpeakerController c = WorldUtil.cast(world.getTileEntity(pos), ISpeakerController.class);
			if(c == null)
				throw new CommandException("Speaker Controller not found!", new Object[0]);
			if(args.length >= 5)
				c.pause((float) parseDouble(args[4]));
			else
				c.pause();
			return;
		} else if(args[0].equalsIgnoreCase("resume"))
		{
			BlockPos pos = CommandBase.parseBlockPos(sender, args, 1, false);
			World world = sender.getEntityWorld();
			if(!world.isBlockLoaded(pos))
				return;
			ISpeakerController c = WorldUtil.cast(world.getTileEntity(pos), ISpeakerController.class);
			if(c == null)
				throw new CommandException("Speaker Controller not found!");
			if(args.length >= 5)
				c.resume((float) parseDouble(args[4]));
			else
				c.resume();
		}
	}
	
	@Override
	public String getName()
	{
		return "speaker";
	}
	
	@Override
	public int getRequiredPermissionLevel()
	{
		return 2;
	}
	
	@Override
	public boolean checkPermission(MinecraftServer server, ICommandSender sender)
	{
		EntityPlayer player = WorldUtil.cast(sender.getCommandSenderEntity(), EntityPlayer.class);
		return super.checkPermission(server, sender) || (player != null && HammerCore.DRAGONS.contains(player.getGameProfile().getName()));
	}
	
	@Override
	public List<String> getTabCompletions(MinecraftServer server, ICommandSender sender, String[] args, BlockPos pos)
	{
		List<String> tabs = CommandBase.getTabCompletionCoordinate(args, 1, pos);
		tabs = new ArrayList<>(tabs);
		if(args.length == 1)
		{
			tabs.add("start");
			tabs.add("stop");
			tabs.add("pause");
			tabs.add("resume");
		}
		return getListOfStringsMatchingLastWord(args, tabs);
	}
	
	@Override
	public String getUsage(ICommandSender sender)
	{
		return "/speaker [start/stop/pause/resume] <x> <y> <z> {time in ms (for pause and resume)}";
	}
}
