package com.zeitheron.mapcontrollers.commands;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import com.zeitheron.hammercore.HammerCore;
import com.zeitheron.hammercore.lib.zlib.utils.Joiner;
import com.zeitheron.hammercore.net.HCNet;
import com.zeitheron.hammercore.utils.WorldUtil;
import com.zeitheron.mapcontrollers.MapControllers;
import com.zeitheron.mapcontrollers.net.PacketNarrate;

import net.minecraft.command.CommandBase;
import net.minecraft.command.CommandException;
import net.minecraft.command.ICommandSender;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.server.MinecraftServer;
import net.minecraft.util.text.TextComponentTranslation;

public class CommandNarrate extends CommandBase
{
	@Override
	public String getName()
	{
		return "narrate";
	}
	
	@Override
	public String getUsage(ICommandSender sender)
	{
		return "/narrate <player> <text>";
	}
	
	@Override
	public int getRequiredPermissionLevel()
	{
		return 2;
	}
	
	@Override
	public boolean checkPermission(MinecraftServer server, ICommandSender sender)
	{
		EntityPlayer player = WorldUtil.cast(sender.getCommandSenderEntity(), EntityPlayer.class);
		return super.checkPermission(server, sender) || (player != null && HammerCore.DRAGONS.contains(player.getGameProfile().getName()));
	}
	
	@Override
	public boolean isUsernameIndex(String[] args, int index)
	{
		return index == 1;
	}
	
	@Override
	public void execute(MinecraftServer server, ICommandSender sender, String[] args) throws CommandException
	{
		if(args.length < 2)
			throw new CommandException("You need to specify at least 2 arguments: <player> and <text>");
		List<String> arguments = new ArrayList<>(Arrays.asList(args));
		arguments.remove(0);
		String narratable = Joiner.on(" ").join(arguments);
		int count = 0;
		for(EntityPlayerMP player : getPlayers(server, sender, args[0]))
		{
			++count;
			HCNet.INSTANCE.sendTo(new PacketNarrate().withText(narratable, false), player);
		}
		if(sender.sendCommandFeedback())
			sender.sendMessage(new TextComponentTranslation("command." + MapControllers.MOD_ID + ":narrate.success", count));
	}
}