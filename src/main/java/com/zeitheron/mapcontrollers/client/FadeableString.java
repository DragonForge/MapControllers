package com.zeitheron.mapcontrollers.client;

public class FadeableString
{
	public final String str;
	
	public float fadeIn, fadeOut;
	public int expectedFadeInTicks = 10, expectedFadeOutTicks = 20;
	
	public FadeableString(String str)
	{
		this.str = str;
	}
	
	@Override
	public String toString()
	{
		return str;
	}
}