package com.zeitheron.mapcontrollers.client.narrator;

import java.util.Queue;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.google.common.collect.Queues;
import com.mojang.text2speech.Narrator;
import com.sun.jna.Native;
import com.sun.jna.NativeLibrary;
import com.sun.jna.WString;

public class NarratorNWin implements Narrator
{
	// JNA SPECIFIC CODE
	private static boolean libraryFound = false;
	private static final Logger LOGGER = LogManager.getLogger();
	
	static
	{
		String result = "";
		
		try
		{
			Native.register(SAPIWrapperSolutionDLL.class, NativeLibrary.getInstance("SAPIWrapper_x64"));
			libraryFound = true;
			LOGGER.info("MapControllers' Narrator library for x64 successfully loaded");
		} catch(final UnsatisfiedLinkError e)
		{
			result += "ERROR: Couldn't load MapControllers' Narrator library : " + e.getMessage() + "\n";
		} catch(final Throwable e)
		{
			result += "ERROR: Generic error while loading MapControllers' narrator : " + e.getMessage() + "\n";
		}
		
		if(!libraryFound)
		{
			try
			{
				Native.register(SAPIWrapperSolutionDLL.class, NativeLibrary.getInstance("SAPIWrapper_x86"));
				libraryFound = true;
				LOGGER.info("Narrator library for x86 successfully loaded");
			} catch(final UnsatisfiedLinkError e)
			{
				result += "ERROR : Couldn't load Narrator library : " + e.getMessage() + "\n";
			} catch(final Throwable e)
			{
				result += "ERROR : Generic error while loading narrator : " + e.getMessage() + "\n";
			}
		}
		
		if(!libraryFound)
		{
			LOGGER.warn(result);
		}
	}
	
	private static class SAPIWrapperSolutionDLL
	{
		public static native long say(WString msg);
	}
	// END OF JNA SPECIFIC CODE
	
	public NarratorNWin()
	{
	}
	
	@Override
	public void say(final String msg)
	{
		if(libraryFound)
			SAPIWrapperSolutionDLL.say(new WString(msg.replaceAll("[<>]", "")));
	}
	
	@Override
	public void clear()
	{
	}
	
	@Override
	public boolean active()
	{
		return libraryFound;
	}
}