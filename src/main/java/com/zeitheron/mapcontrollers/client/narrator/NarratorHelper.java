package com.zeitheron.mapcontrollers.client.narrator;

import java.util.Locale;

import com.mojang.text2speech.Narrator;
import com.mojang.text2speech.NarratorDummy;
import com.zeitheron.mapcontrollers.api.Narratable;

public class NarratorHelper
{
	public static Narratable sayable(String text)
	{
		return new Narratable(false, narratable ->
		{
			narratable.set(false);
			sayBlocking(text);
			narratable.set(true);
		});
	}
	
	public static void sayBlocking(String text)
	{
		if(blockingNarrator.active())
			blockingNarrator.say(text);
	}
	
	public static final Narrator asyncNarrator = Narrator.getNarrator();
	public static final Narrator blockingNarrator = getNarrator();
	
	private static Narrator getNarrator()
	{
		final String osName = System.getProperty("os.name").toLowerCase(Locale.ROOT);
		if(osName.contains("linux"))
		{
			setJNAPath(":");
			return new NarratorNLinux();
		} else if(osName.contains("win"))
		{
			setJNAPath(";");
			return new NarratorNWin();
		} else if(osName.contains("mac"))
		{
			setJNAPath(":");
			return new NarratorNOSX();
		} else
			return new NarratorDummy();
	}
	
	static void setJNAPath(String sep)
	{
		System.setProperty("jna.library.path", System.getProperty("jna.library.path") + sep + "./src/natives/resources/");
		System.setProperty("jna.library.path", System.getProperty("jna.library.path") + sep + System.getProperty("java.library.path"));
	}
}