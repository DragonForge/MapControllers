package com.zeitheron.mapcontrollers.client.narrator;

import java.util.Queue;

import com.google.common.collect.Queues;
import com.mojang.text2speech.Narrator;

import ca.weblite.objc.Client;
import ca.weblite.objc.NSObject;
import ca.weblite.objc.Proxy;
import ca.weblite.objc.annotations.Msg;

public class NarratorNOSX extends NSObject implements Narrator
{
	private final Proxy synth = Client.getInstance().sendProxy("NSSpeechSynthesizer", "alloc");
	private boolean speaking;
	
	private final Queue<String> queue = Queues.newConcurrentLinkedQueue();
	
	public NarratorNOSX()
	{
		super("NSObject");
		synth.send("init");
		synth.send("setDelegate:", this);
	}
	
	private void startSpeaking(final String message)
	{
		synth.send("startSpeakingString:", message);
	}
	
	@Msg(selector = "speechSynthesizer:didFinishSpeaking:", signature = "v@:B")
	public void didFinishSpeaking(final boolean naturally)
	{
		if(queue.isEmpty())
		{
			speaking = false;
		} else
		{
			startSpeaking(queue.poll());
		}
	}
	
	@Override
	public void say(final String msg)
	{
		if(speaking)
		{
			queue.offer(msg);
		} else
		{
			speaking = true;
			startSpeaking(msg);
		}
		
		while(speaking)
			try
			{
				Thread.sleep(100L);
			} catch(InterruptedException e)
			{
				e.printStackTrace();
			}
	}
	
	@Override
	public void clear()
	{
		queue.clear();
	}
	
	@Override
	public boolean active()
	{
		return true;
	}
}