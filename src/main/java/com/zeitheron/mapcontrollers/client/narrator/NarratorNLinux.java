package com.zeitheron.mapcontrollers.client.narrator;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.mojang.text2speech.Narrator;
import com.sun.jna.Native;
import com.sun.jna.NativeLibrary;

public class NarratorNLinux implements Narrator
{
	// JNA SPECIFIC CODE
	private static boolean libraryFound = false;
	private static final Logger LOGGER = LogManager.getLogger();
	
	static
	{
		try
		{
			Native.register(FliteLibrary.class, NativeLibrary.getInstance("fliteWrapper"));
			FliteLibrary.init();
			libraryFound = true;
			LOGGER.info("MapControllers' Narrator library successfully loaded");
		} catch(final UnsatisfiedLinkError e)
		{
			LOGGER.warn("ERROR : Couldn't load MapControllers' Narrator library : " + e.getMessage());
		} catch(final Throwable e)
		{
			LOGGER.warn("ERROR : Generic error while loading MapControllers' narrator : " + e.getMessage());
		}
	}
	
	private static class FliteLibrary
	{
		public static native int init();
		
		public static native float say(String text);
	}
	// END OF JNA SPECIFIC CODE
	
	@Override
	public void say(final String msg)
	{
		if(libraryFound)
			FliteLibrary.say(msg.replaceAll("[<>]", ""));
	}
	
	@Override
	public void clear()
	{
	}
	
	@Override
	public boolean active()
	{
		return libraryFound;
	}
}