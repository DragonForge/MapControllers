package com.zeitheron.mapcontrollers.client;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.util.HashMap;
import java.util.Map;
import java.util.function.Supplier;
import java.util.stream.Collectors;

import com.zeitheron.mapcontrollers.client.sounds.ISoundOwner;
import com.zeitheron.mapcontrollers.client.sounds.SoundOwnerNone;
import com.zeitheron.mapcontrollers.client.sounds.SoundOwnerSoundLib;
import com.zeitheron.mapcontrollers.proxy.ClientProxy;

import net.minecraft.client.Minecraft;
import net.minecraft.util.math.BlockPos;
import net.minecraftforge.fml.common.eventhandler.SubscribeEvent;
import net.minecraftforge.fml.common.gameevent.TickEvent;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;

@SideOnly(Side.CLIENT)
public class SoundManager
{
	public static Map<Long, ISoundOwner> sounds = new HashMap<>();
	public static Class<? extends ISoundOwner> useSound = SoundOwnerNone.class;
	
	static
	{
		System.out.println("Obtaining ISoundOwner...");
		try
		{
			Class.forName("com.zeitheron.sound.SoundLib");
			useSound = SoundOwnerSoundLib.class;
			System.out.println("Found SoundLib, using " + useSound.getName());
		} catch(Throwable err)
		{
			System.out.println("SoundLib not found. Switching to no-sound (" + useSound.getName() + ")");
		}
	}
	
	public static void pauseSound(BlockPos pos)
	{
		ISoundOwner owner = sounds.get(pos.toLong());
		if(owner != null)
			owner.pause();
	}
	
	public static void pauseSound(BlockPos pos, float fadeout)
	{
		ISoundOwner owner = sounds.get(pos.toLong());
		if(owner != null)
			owner.pause(fadeout);
	}
	
	public static void resumeSound(BlockPos pos, float fadein)
	{
		ISoundOwner owner = sounds.get(pos.toLong());
		if(owner != null)
			owner.resume(fadein);
	}
	
	public static void playSound(byte[] sound, BlockPos pos, float rad)
	{
		byte[] is = sound.clone();
		SoundManager.playSound(() -> new ByteArrayInputStream(is), pos, rad);
	}
	
	public static void playSound(Supplier<InputStream> sound, BlockPos pos, float rad)
	{
		ISoundOwner owner = sounds.get(pos.toLong());
		if(owner != null)
			owner.stop();
		try
		{
			owner = useSound.newInstance();
		} catch(Throwable err)
		{
			System.out.println("Error in using ISoundOwner - " + useSound.getName() + " - switching to no-sound (" + SoundOwnerNone.class + ")");
			useSound = SoundOwnerNone.class;
			try
			{
				owner = useSound.newInstance();
			} catch(Throwable err2)
			{
				System.err.println("Failed to use no-sound. Who is using ASM????");
				throw new RuntimeException(err2);
			}
		}
		owner.setSource(sound);
		owner.setPos(pos);
		owner.setBlockHearRad(rad);
		owner.start();
		sounds.put(pos.toLong(), owner);
	}
	
	public static void resumeSound(BlockPos pos)
	{
		ISoundOwner owner = sounds.get(pos.toLong());
		if(owner != null)
			owner.resume();
	}
	
	public static void stopSound(BlockPos pos)
	{
		ISoundOwner owner = sounds.get(pos.toLong());
		if(owner != null)
			owner.stop();
	}
	
	@SubscribeEvent
	public void tick(TickEvent.ClientTickEvent e)
	{
		if(Minecraft.getMinecraft().player == null)
		{
			sounds.clear();
			ClientProxy.renderSubtitles.clear();
		}
		if(e.phase == TickEvent.Phase.START)
			sounds.values().retainAll(sounds.values().stream().filter(ISoundOwner::update).collect(Collectors.toList()));
	}
}