package com.zeitheron.mapcontrollers.client.fx;

import net.minecraft.client.particle.Barrier;
import net.minecraft.item.Item;
import net.minecraft.world.World;

public class ParticleScreen extends Barrier
{
	public ParticleScreen(World worldIn, double x, double y, double z, Item i)
	{
		super(worldIn, x, y, z, i);
	}
}