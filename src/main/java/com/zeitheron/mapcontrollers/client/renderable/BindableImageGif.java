package com.zeitheron.mapcontrollers.client.renderable;

import java.io.IOException;
import java.net.URLConnection;
import java.util.Objects;

import com.zeitheron.hammercore.client.utils.UtilsFX;
import com.zeitheron.hammercore.lib.zlib.utils.Threading;
import com.zeitheron.mapcontrollers.client.gifdec.GLGifInfo;
import com.zeitheron.mapcontrollers.client.gifdec.GifDecoder;
import com.zeitheron.mapcontrollers.tile.TileDisplayController;

public class BindableImageGif implements IBindableImage
{
	public final String url;
	
	public GLGifInfo info;
	
	public BindableImageGif(String url, URLConnection urlc)
	{
		this.url = url;
		Threading.createAndStart("DwnGIF$" + url, () ->
		{
			try
			{
				info = GifDecoder.decodeGifIntoVideoMem(urlc.getInputStream());
			} catch(IOException e)
			{
				e.printStackTrace();
			}
		});
	}
	
	@Override
	public boolean glBind(TileDisplayController screen, double timeInTicks)
	{
		if(info == null)
		{
			UtilsFX.bindTexture(ImagePuller.nosignal_gl);
			return true;
		}
		float mod = 10F;
		long milliTime = System.currentTimeMillis() + Math.abs(screen.getPos().toLong());
		if(milliTime < 0L)
			milliTime = -milliTime;
		int frame = info.getFrameFromMS(milliTime);
		if(frame < 0)
		{
			UtilsFX.bindTexture(ImagePuller.nosignal_gl);
			return true;
		}
		UtilsFX.bindTexture(info.tex[frame % info.tex.length]);
		return true;
	}
	
	@Override
	public boolean isSameURL(String url)
	{
		return Objects.equals(url, this.url);
	}
}