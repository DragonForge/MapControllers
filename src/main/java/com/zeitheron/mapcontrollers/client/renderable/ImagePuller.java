package com.zeitheron.mapcontrollers.client.renderable;

import java.awt.image.BufferedImage;
import java.util.HashMap;
import java.util.Map;

import javax.annotation.Nullable;
import javax.imageio.ImageIO;

import com.zeitheron.hammercore.client.utils.texture.TexLocUploader;
import com.zeitheron.hammercore.lib.zlib.io.IOUtils;
import com.zeitheron.mapcontrollers.MapControllers;

import net.minecraft.client.Minecraft;
import net.minecraft.client.renderer.texture.AbstractTexture;
import net.minecraft.client.renderer.texture.ITextureObject;
import net.minecraft.client.renderer.texture.TextureUtil;
import net.minecraft.util.ResourceLocation;

/**
 * This class downloads images from http and https without blocking the caller
 * thread. Useful for rendering
 */
public class ImagePuller
{
	private static BufferedImage nosignal = null;
	public static ResourceLocation nosignal_gl = new ResourceLocation(MapControllers.MOD_ID, "textures/nosignal.png");
	
	public static final Map<String, Thread> DWN = new HashMap<>();
	public static final Map<String, ResourceLocation> GL_IMAGES = new HashMap<>();
	
	@Nullable
	public static BufferedImage noSignalBI()
	{
		if(nosignal == null)
			try
			{
				nosignal = ImageIO.read(MapControllers.class.getResourceAsStream("/assets/" + MapControllers.MOD_ID + "/textures/nosignal.png"));
			} catch(Throwable err)
			{
			}
		return nosignal;
	}
	
	public static ResourceLocation getImageGL(String url)
	{
		if(url == null || url.isEmpty() || !url.toLowerCase().startsWith("http"))
			return nosignal_gl;
		if(DWN.get(url) != null)
			return nosignal_gl;
		else if(GL_IMAGES.get(url) == null)
			download(url);
		return GL_IMAGES.getOrDefault(url, nosignal_gl);
	}
	
	public static boolean bind(String url)
	{
		ResourceLocation gl = getImageGL(url);
		if(gl != null)
			Minecraft.getMinecraft().getTextureManager().bindTexture(gl);
		return gl != null;
	}
	
	private static void download(String url)
	{
		ResourceLocation rl = getLocationForURL(url);
		
		MapControllers.LOG.info("Start download " + rl);
		
		Thread t = new Thread(() ->
		{
			BufferedImage img = IOUtils.downloadPicture(url);
			
			if(img == null)
				img = noSignalBI();
			
			final BufferedImage icon = img;
			
			Minecraft.getMinecraft().addScheduledTask(() ->
			{
				TexLocUploader.upload(rl, icon);
				GL_IMAGES.put(url, rl);
			});
			
			DWN.remove(url);
		});
		
		t.start();
		DWN.put(url, t);
	}
	
	public static ResourceLocation getLocationForURL(String url)
	{
		return new ResourceLocation(MapControllers.MOD_ID, url.substring(url.indexOf("://") + 3));
	}
	
	public static void cleanupAll()
	{
		Minecraft.getMinecraft().addScheduledTask(() ->
		{
			MapControllers.LOG.info("Cleaning " + GL_IMAGES.size() + " display textures from VRAM...");
			GL_IMAGES.values().forEach(r -> deleteGlTexture(Minecraft.getMinecraft().getTextureManager().mapTextureObjects.remove(r)));
			MapControllers.LOG.info("Interrupting " + DWN.size() + " display download threads...");
			DWN.values().forEach(Thread::interrupt);
			GL_IMAGES.clear();
			DWN.clear();
			MapControllers.LOG.info("Congrats! All textures have been cleaned and all threads were stopped!");
		});
	}
	
	public static void deleteGlTexture(ITextureObject tex)
	{
		if(tex == null)
			return;
		if(tex instanceof AbstractTexture)
			((AbstractTexture) tex).deleteGlTexture();
		else if(tex.getGlTextureId() != -1)
			TextureUtil.deleteTexture(tex.getGlTextureId());
	}
}