package com.zeitheron.mapcontrollers.client.renderable;

import java.io.IOException;
import java.net.URL;
import java.net.URLConnection;

import com.zeitheron.mapcontrollers.tile.TileDisplayController;

public interface IBindableImage
{
	boolean glBind(TileDisplayController screen, double timeInTicks);
	
	boolean isSameURL(String url);
	
	static IBindableImage probe(String url)
	{
		try
		{
			URLConnection urlc = new URL(url).openConnection();
			if(urlc.getContentType().equals("image/gif"))
				return new BindableImageGif(url, urlc);
		} catch(IOException e)
		{
			e.printStackTrace();
		}
		return new BindableImageStt(url);
	}
}