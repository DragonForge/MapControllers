package com.zeitheron.mapcontrollers.client.renderable;

import java.util.Objects;

import com.zeitheron.mapcontrollers.tile.TileDisplayController;

public class BindableImageStt implements IBindableImage
{
	public final String url;
	
	public BindableImageStt(String url)
	{
		this.url = url;
	}
	
	@Override
	public boolean glBind(TileDisplayController screen, double timeInTicks)
	{
		return ImagePuller.bind(url);
	}
	
	@Override
	public boolean isSameURL(String url)
	{
		return Objects.equals(url, this.url);
	}
}