package com.zeitheron.mapcontrollers.client.sounds;

import java.io.InputStream;
import java.util.function.Supplier;

import net.minecraft.util.math.BlockPos;

public interface ISoundOwner
{
	void pause();
	
	void pause(float fadeout);
	
	void resume();
	
	void resume(float fadein);
	
	void setBlockHearRad(float var1);
	
	void setPos(BlockPos var1);
	
	void setSource(Supplier<InputStream> var1);
	
	void start();
	
	void stop();
	
	boolean update();
}
