package com.zeitheron.mapcontrollers.client.sounds;

import java.io.InputStream;
import java.util.function.Supplier;

import net.minecraft.util.math.BlockPos;

public class SoundOwnerNone implements ISoundOwner
{
	@Override
	public void pause()
	{
	}
	
	@Override
	public void resume()
	{
	}
	
	@Override
	public void setBlockHearRad(float rad)
	{
	}
	
	@Override
	public void setPos(BlockPos pos)
	{
	}
	
	@Override
	public void setSource(Supplier<InputStream> in)
	{
	}
	
	@Override
	public void start()
	{
	}
	
	@Override
	public void stop()
	{
	}
	
	@Override
	public boolean update()
	{
		return false;
	}

	@Override
	public void pause(float fadeout)
	{
	}

	@Override
	public void resume(float fadein)
	{
	}
}
