package com.zeitheron.mapcontrollers.client.sounds;

import java.io.IOException;
import java.io.InputStream;
import java.util.function.Supplier;

import javax.sound.sampled.LineUnavailableException;
import javax.sound.sampled.UnsupportedAudioFileException;

import com.zeitheron.sound.RepeatableSound;

import net.minecraft.client.Minecraft;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.util.SoundCategory;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.MathHelper;

public class SoundOwnerSoundLib implements ISoundOwner
{
	public BlockPos pos;
	public float rad = 128.0f;
	public RepeatableSound sound;
	
	public long startMs = 0;
	public long durationMs = 0;
	public boolean msAction;
	
	@Override
	public void pause()
	{
		if(sound != null)
		{
			sound.stop();
			msAction = false;
			startMs = System.currentTimeMillis();
			durationMs = 0;
		}
	}
	
	@Override
	public void resume()
	{
		resume(0F);
	}
	
	@Override
	public void setBlockHearRad(float rad)
	{
		this.rad = rad;
	}
	
	@Override
	public void setPos(BlockPos pos)
	{
		this.pos = pos;
	}
	
	@Override
	public void setSource(Supplier<InputStream> in)
	{
		try
		{
			sound = new RepeatableSound(() -> (InputStream) in.get());
		} catch(IOException | LineUnavailableException | UnsupportedAudioFileException e)
		{
			e.printStackTrace();
		}
	}
	
	@Override
	public void start()
	{
		if(sound != null)
		{
			sound.play();
			try
			{
				resume();
			} catch(Throwable err)
			{
			}
		}
	}
	
	@Override
	public void stop()
	{
		if(sound != null)
			sound.dispose();
	}
	
	@Override
	public boolean update()
	{
		EntityPlayer player = Minecraft.getMinecraft().player;
		
		if(player == null)
		{
			sound.stop();
			sound.dispose();
			return false;
		}
		
		if(sound != null && pos != null)
		{
			double rad = Math.sqrt(player.getDistanceSq(pos));
			float vol = MathHelper.clamp(1 - (float) (rad / this.rad), 0F, 1F);
			float val = vol * Minecraft.getMinecraft().gameSettings.getSoundLevel(SoundCategory.RECORDS);
			long ctime = System.currentTimeMillis();
			float coef = ctime >= startMs + durationMs ? 1F : ctime >= startMs ? 1F - (startMs + durationMs - ctime) / (float) durationMs : 0F;
			coef = (msAction ? coef : 1 - coef);
			if(coef == 0F && !msAction)
				pause();
			sound.setVolume(val * coef);
		}
		
		return sound != null && !sound.isDonePlaying();
	}
	
	@Override
	public void pause(float fadeout)
	{
		msAction = false;
		startMs = System.currentTimeMillis();
		durationMs = (long) fadeout;
	}
	
	@Override
	public void resume(float fadein)
	{
		msAction = true;
		startMs = System.currentTimeMillis();
		durationMs = (long) fadein;
		start();
	}
}