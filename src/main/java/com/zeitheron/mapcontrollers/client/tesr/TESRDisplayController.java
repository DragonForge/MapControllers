package com.zeitheron.mapcontrollers.client.tesr;

import org.lwjgl.opengl.GL11;

import com.zeitheron.hammercore.annotations.AtTESR;
import com.zeitheron.hammercore.client.render.tesr.TESR;
import com.zeitheron.mapcontrollers.client.renderable.IBindableImage;
import com.zeitheron.mapcontrollers.tile.TileDisplayController;

import net.minecraft.client.Minecraft;
import net.minecraft.client.renderer.BufferBuilder;
import net.minecraft.client.renderer.GlStateManager;
import net.minecraft.client.renderer.OpenGlHelper;
import net.minecraft.client.renderer.RenderHelper;
import net.minecraft.client.renderer.Tessellator;
import net.minecraft.client.renderer.vertex.DefaultVertexFormats;
import net.minecraft.profiler.Profiler;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.math.Vec3d;

@AtTESR(TileDisplayController.class)
public class TESRDisplayController extends TESR<TileDisplayController>
{
	@Override
	public void renderTileEntityAt(TileDisplayController te, double x, double y, double z, float partialTicks, ResourceLocation destroyStage, float alpha)
	{
		Profiler profiler = Minecraft.getMinecraft().mcProfiler;
		profiler.startSection("Display Controllers");
		
		IBindableImage img = te.getBindable();
		if(img != null && img.glBind(te, te.ticksExisted + (double) partialTicks))
		{
			Vec3d t = te.getTranslation().addVector(x, y, z);
			Vec3d rotation = te.getRotation();
			Vec3d scale = te.getScale();
			
			GL11.glPushMatrix();
			GlStateManager.disableLighting();
			RenderHelper.disableStandardItemLighting();
			GlStateManager.blendFunc(GlStateManager.SourceFactor.SRC_ALPHA, GlStateManager.DestFactor.ONE_MINUS_SRC_ALPHA);
			GlStateManager.enableBlend();
			GlStateManager.disableCull();
			
			if(Minecraft.isAmbientOcclusionEnabled())
				GlStateManager.shadeModel(7425);
			else
				GlStateManager.shadeModel(7424);
			
			GL11.glTranslated(t.x, t.y, t.z);
			GL11.glRotated(rotation.x, 1, 0, 0);
			GL11.glRotated(rotation.y, 0, 1, 0);
			GL11.glRotated(rotation.z, 0, 0, 1);
			GL11.glScaled(scale.x, scale.y, scale.z);
			
			int i = te.getWorld().getCombinedLight(te.getPos(), 0);
			int j = i % 65536;
			int k = i / 65536;
			OpenGlHelper.setLightmapTextureCoords(OpenGlHelper.lightmapTexUnit, j, k);
			
			BufferBuilder bb = Tessellator.getInstance().getBuffer();
			bb.begin(GL11.GL_QUADS, DefaultVertexFormats.POSITION_TEX_COLOR);
			
			bb.pos(0, 0, 0).tex(1, 1).color(1F, 1F, 1F, 1F).endVertex();
			bb.pos(0, 1, 0).tex(1, 0).color(1F, 1F, 1F, 1F).endVertex();
			bb.pos(1, 1, 0).tex(0, 0).color(1F, 1F, 1F, 1F).endVertex();
			bb.pos(1, 0, 0).tex(0, 1).color(1F, 1F, 1F, 1F).endVertex();
			
			Tessellator.getInstance().draw();
			RenderHelper.enableStandardItemLighting();
			GL11.glPopMatrix();
		}
		
		profiler.endSection();
	}
}