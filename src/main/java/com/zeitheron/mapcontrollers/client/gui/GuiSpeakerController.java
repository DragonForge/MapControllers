package com.zeitheron.mapcontrollers.client.gui;

import java.io.IOException;

import org.lwjgl.input.Keyboard;

import com.zeitheron.hammercore.client.gui.GuiCentered;
import com.zeitheron.hammercore.client.gui.impl.container.ContainerEmpty;
import com.zeitheron.hammercore.net.HCNet;
import com.zeitheron.mapcontrollers.net.PacketSetSource;
import com.zeitheron.mapcontrollers.tile.TileSpeakerController;

import net.minecraft.client.gui.GuiButton;
import net.minecraft.client.gui.GuiTextField;
import net.minecraft.client.gui.inventory.GuiContainer;

public class GuiSpeakerController extends GuiCentered
{
	public final TileSpeakerController tile;
	public GuiTextField source;
	public GuiSlider volume;
	public GuiButton save;
	public GuiButton cancel;
	
	public GuiSpeakerController(TileSpeakerController tile)
	{
		this.tile = tile;
		xSize = 250;
		ySize = 50;
	}
	
	@Override
	protected void actionPerformed(GuiButton button) throws IOException
	{
		if(button == save)
			HCNet.INSTANCE.sendToServer(new PacketSetSource(tile.getPos(), source.getText(), volume.sliderValue * 512F));
		if(button == cancel)
			mc.displayGuiScreen(null);
	}
	
	@Override
	protected void drawGuiContainerBackgroundLayer(float partialTicks, int mouseX, int mouseY)
	{
		drawDefaultBackground();
		source.drawTextBox();
		volume.displayString = "Hearable Distance: " + Math.round(volume.sliderValue * 512F);
	}
	
	@Override
	public void initGui()
	{
		super.initGui();
		
		int guiLeft = (int) this.guiLeft, guiTop = (int) this.guiTop, xSize = (int) this.xSize, ySize = (int) this.ySize;
		
		source = new GuiTextField(0, fontRenderer, guiLeft + xSize / 2 - 125, guiTop, 250, 20);
		save = new GuiButton(0, guiLeft + (xSize - 150) / 2, guiTop + ySize, 40, 20, "Save");
		cancel = new GuiButton(1, guiLeft + (xSize - 150) / 2 + 110, guiTop + ySize, 40, 20, "Close");
		volume = new GuiSlider(2, guiLeft + (xSize - 150) / 2, guiTop + 25, "Volume", 0.0f, 1.0f);
		source.setMaxStringLength(10000);
		source.setText(tile.sound);
		volume.sliderValue = tile.rad / 512F;
		this.addButton(save);
		this.addButton(cancel);
		this.addButton((GuiButton) volume);
		Keyboard.enableRepeatEvents(true);
	}
	
	@Override
	protected void keyTyped(char typedChar, int keyCode) throws IOException
	{
		if(!source.textboxKeyTyped(typedChar, keyCode))
			super.keyTyped(typedChar, keyCode);
	}
	
	@Override
	protected void mouseClicked(int mouseX, int mouseY, int mouseButton) throws IOException
	{
		super.mouseClicked(mouseX, mouseY, mouseButton);
		source.mouseClicked(mouseX, mouseY, mouseButton);
	}
	
	@Override
	public void onGuiClosed()
	{
		super.onGuiClosed();
		Keyboard.enableRepeatEvents(false);
	}
}