package com.zeitheron.mapcontrollers.client.gui;

import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.GuiButton;
import net.minecraft.client.renderer.GlStateManager;
import net.minecraft.util.math.MathHelper;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;

@SideOnly(Side.CLIENT)
public class GuiSlider extends GuiButton
{
	public float sliderValue = 1.0f;
	public boolean dragging;
	public final float minValue;
	public final float maxValue;
	
	public GuiSlider(int buttonId, int x, int y)
	{
		this(buttonId, x, y, "?", 0.0f, 1.0f);
	}
	
	public GuiSlider(int buttonId, int x, int y, String text, float minValueIn, float maxValue)
	{
		super(buttonId, x, y, 150, 20, "");
		minValue = minValueIn;
		this.maxValue = maxValue;
		displayString = text;
	}
	
	@Override
	protected int getHoverState(boolean mouseOver)
	{
		return 0;
	}
	
	@Override
	protected void mouseDragged(Minecraft mc, int mouseX, int mouseY)
	{
		if(visible)
		{
			if(dragging)
			{
				sliderValue = (float) (mouseX - (x + 4)) / (float) (width - 8);
				sliderValue = MathHelper.clamp(sliderValue, 0.0f, 1.0f);
				sliderValue = minValue + sliderValue * (maxValue - minValue);
			}
			mc.getTextureManager().bindTexture(BUTTON_TEXTURES);
			GlStateManager.color(1.0f, 1.0f, 1.0f, 1.0f);
			this.drawTexturedModalRect(x + (int) (sliderValue * (width - 8)), y, 0, 66, 4, 20);
			this.drawTexturedModalRect(x + (int) (sliderValue * (width - 8)) + 4, y, 196, 66, 4, 20);
		}
	}
	
	@Override
	public boolean mousePressed(Minecraft mc, int mouseX, int mouseY)
	{
		if(super.mousePressed(mc, mouseX, mouseY))
		{
			sliderValue = (float) (mouseX - (x + 4)) / (float) (width - 8);
			sliderValue = MathHelper.clamp(sliderValue, 0.0f, 1.0f);
			sliderValue = minValue + sliderValue * (maxValue - minValue);
			dragging = true;
			return true;
		}
		return false;
	}
	
	@Override
	public void mouseReleased(int mouseX, int mouseY)
	{
		dragging = false;
	}
}
