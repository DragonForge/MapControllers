package com.zeitheron.mapcontrollers.client.gui;

import java.io.IOException;

import org.lwjgl.input.Keyboard;

import com.zeitheron.hammercore.client.gui.GuiCentered;
import com.zeitheron.hammercore.net.HCNet;
import com.zeitheron.mapcontrollers.net.PacketSetSource;
import com.zeitheron.mapcontrollers.tile.TileNarratorController;

import net.minecraft.client.gui.GuiButton;
import net.minecraft.client.gui.GuiTextField;

public class GuiNarratorController extends GuiCentered
{
	public final TileNarratorController tile;
	public GuiTextField source;
	public GuiSlider volume;
	public GuiButton save;
	public GuiButton cancel;
	
	public GuiNarratorController(TileNarratorController tile)
	{
		this.tile = tile;
		xSize = 250;
		ySize = 50;
	}
	
	@Override
	protected void actionPerformed(GuiButton button) throws IOException
	{
		if(button == save)
			HCNet.INSTANCE.sendToServer(new PacketSetSource(tile.getPos(), source.getText(), volume.sliderValue * 256F));
		if(button == cancel)
			mc.displayGuiScreen(null);
	}
	
	@Override
	protected void drawGuiContainerBackgroundLayer(float partialTicks, int mouseX, int mouseY)
	{
		drawDefaultBackground();
		source.drawTextBox();
		volume.displayString = "Hearable Distance: " + Math.round(volume.sliderValue * 256F);
	}
	
	@Override
	public void initGui()
	{
		super.initGui();
		
		int guiLeft = (int) this.guiLeft, guiTop = (int) this.guiTop, xSize = (int) this.xSize, ySize = (int) this.ySize;
		
		source = new GuiTextField(0, fontRenderer, guiLeft + xSize / 2 - 125, guiTop, 250, 20);
		save = new GuiButton(0, guiLeft + (xSize - 150) / 2, guiTop + ySize, 40, 20, "Save");
		cancel = new GuiButton(1, guiLeft + (xSize - 150) / 2 + 110, guiTop + ySize, 40, 20, "Close");
		volume = new GuiSlider(2, guiLeft + (xSize - 150) / 2, guiTop + 25, "Volume", 0F, 1F);
		source.setMaxStringLength(16 * 1024);
		source.setText(tile.text);
		volume.sliderValue = tile.hearableDistance / 256F;
		this.addButton(save);
		this.addButton(cancel);
		this.addButton(volume);
		Keyboard.enableRepeatEvents(true);
	}
	
	@Override
	protected void keyTyped(char typedChar, int keyCode) throws IOException
	{
		if(!source.textboxKeyTyped(typedChar, keyCode))
			super.keyTyped(typedChar, keyCode);
	}
	
	@Override
	protected void mouseClicked(int mouseX, int mouseY, int mouseButton) throws IOException
	{
		super.mouseClicked(mouseX, mouseY, mouseButton);
		source.mouseClicked(mouseX, mouseY, mouseButton);
	}
	
	@Override
	public void onGuiClosed()
	{
		super.onGuiClosed();
		Keyboard.enableRepeatEvents(false);
	}
}