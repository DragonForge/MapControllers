package com.zeitheron.mapcontrollers.client.gui;

import java.io.IOException;

import org.lwjgl.input.Keyboard;

import com.zeitheron.hammercore.client.gui.GuiCentered;
import com.zeitheron.hammercore.client.utils.texture.gui.DynGuiTex;
import com.zeitheron.hammercore.client.utils.texture.gui.GuiTexBakery;
import com.zeitheron.hammercore.net.HCNet;
import com.zeitheron.hammercore.net.internal.PacketSyncSyncableTile;
import com.zeitheron.mapcontrollers.tile.TileDisplayController;

import net.minecraft.client.gui.GuiButton;
import net.minecraft.client.gui.GuiTextField;
import net.minecraft.client.renderer.RenderHelper;
import net.minecraft.util.math.Vec3d;
import net.minecraft.util.text.TextComponentString;

public class GuiDisplayController extends GuiCentered
{
	public DynGuiTex tex;
	public TileDisplayController t;
	public GuiTextField[] fields = new GuiTextField[10];
	
	public GuiDisplayController(TileDisplayController t)
	{
		this.t = t;
		
		xSize = 200;
		ySize = 127;
	}
	
	@Override
	public void initGui()
	{
		super.initGui();
		
		Keyboard.enableRepeatEvents(true);
		
		int gl = (int) guiLeft, gt = (int) guiTop, xs = (int) xSize, ys = (int) ySize;
		
		GuiTexBakery b = GuiTexBakery.start();
		b.body(0, 0, xs, ys);
		tex = b.bake();
		
		addButton(new GuiButton(0, gl + 8, gt + ys - 24, xs - 16, 20, "Done"));
		
		fields[0] = new GuiTextField(0, fontRenderer, gl + 8, gt + 8, xs - 16, 20);
		fields[0].setMaxStringLength(2048);
		fields[0].setText(t.image);
		
		gl += (xSize - 114) / 2;
		
		//
		
		fields[1] = new GuiTextField(1, fontRenderer, gl + 8, gt + 32, 30, 20);
		fields[1].setMaxStringLength(10);
		fields[1].setText(t.getScale().x + "");
		
		fields[2] = new GuiTextField(2, fontRenderer, gl + 42, gt + 32, 30, 20);
		fields[2].setMaxStringLength(10);
		fields[2].setText(t.getScale().y + "");
		
		fields[3] = new GuiTextField(3, fontRenderer, gl + 76, gt + 32, 30, 20);
		fields[3].setMaxStringLength(10);
		fields[3].setText(t.getScale().z + "");
		
		//
		
		fields[4] = new GuiTextField(4, fontRenderer, gl + 8, gt + 56, 30, 20);
		fields[4].setMaxStringLength(10);
		fields[4].setText(t.getTranslation().x + "");
		
		fields[5] = new GuiTextField(5, fontRenderer, gl + 42, gt + 56, 30, 20);
		fields[5].setMaxStringLength(10);
		fields[5].setText(t.getTranslation().y + "");
		
		fields[6] = new GuiTextField(6, fontRenderer, gl + 76, gt + 56, 30, 20);
		fields[6].setMaxStringLength(10);
		fields[6].setText(t.getTranslation().z + "");
		
		//
		
		fields[7] = new GuiTextField(7, fontRenderer, gl + 8, gt + 80, 30, 20);
		fields[7].setMaxStringLength(10);
		fields[7].setText(t.getRotation().x + "");
		
		fields[8] = new GuiTextField(8, fontRenderer, gl + 42, gt + 80, 30, 20);
		fields[8].setMaxStringLength(10);
		fields[8].setText(t.getRotation().y + "");
		
		fields[9] = new GuiTextField(9, fontRenderer, gl + 76, gt + 80, 30, 20);
		fields[9].setMaxStringLength(10);
		fields[9].setText(t.getRotation().z + "");
	}
	
	@Override
	public void drawScreen(int mouseX, int mouseY, float partialTicks)
	{
		drawDefaultBackground();
		super.drawScreen(mouseX, mouseY, partialTicks);
	}
	
	@Override
	protected void drawGuiContainerBackgroundLayer(float partialTicks, int mouseX, int mouseY)
	{
		tex.render((int) guiLeft, (int) guiTop);
		for(GuiTextField tf : fields)
			if(tf != null)
				tf.drawTextBox();
		for(GuiTextField tf : fields)
			if(tf != null)
				if(!tf.isFocused() && mouseX >= tf.x && mouseY >= tf.y && mouseX < tf.x + tf.width && mouseY < tf.y + tf.height)
					switch(tf.getId())
					{
					case 0:
						drawHoveringText("Image URL", mouseX, mouseY);
					break;
					default:
					break;
					case 1:
						drawHoveringText("Scale X", mouseX, mouseY);
					break;
					case 2:
						drawHoveringText("Scale Y", mouseX, mouseY);
					break;
					case 3:
						drawHoveringText("Scale Z", mouseX, mouseY);
					break;
					case 4:
						drawHoveringText("Translation X", mouseX, mouseY);
					break;
					case 5:
						drawHoveringText("Translation Y", mouseX, mouseY);
					break;
					case 6:
						drawHoveringText("Translation Z", mouseX, mouseY);
					break;
					case 7:
						drawHoveringText("Rotation X", mouseX, mouseY);
					break;
					case 8:
						drawHoveringText("Rotation Y", mouseX, mouseY);
					break;
					case 9:
						drawHoveringText("Rotation Z", mouseX, mouseY);
					break;
					}
				
		RenderHelper.disableStandardItemLighting();
	}
	
	@Override
	protected void mouseClicked(int mouseX, int mouseY, int mouseButton) throws IOException
	{
		boolean prev = false;
		for(GuiTextField tf : fields)
			if(tf != null && tf.mouseClicked(mouseX, mouseY, mouseButton))
				prev = true;
		if(prev)
			return;
		super.mouseClicked(mouseX, mouseY, mouseButton);
	}
	
	@Override
	protected void keyTyped(char typedChar, int keyCode) throws IOException
	{
		for(GuiTextField tf : fields)
			if(tf != null && tf.textboxKeyTyped(typedChar, keyCode))
				return;
		super.keyTyped(typedChar, keyCode);
	}
	
	@Override
	protected void actionPerformed(GuiButton button) throws IOException
	{
		if(button.id == 0)
		{
			t.image = fields[0].getText();
			
			try
			{
				t.scale = new Vec3d(Double.parseDouble(fields[1].getText()), Double.parseDouble(fields[2].getText()), Double.parseDouble(fields[3].getText()));
			} catch(Throwable err)
			{
				mc.player.sendMessage(new TextComponentString("Unable to parse scale!"));
			}
			
			try
			{
				t.translation = new Vec3d(Double.parseDouble(fields[4].getText()), Double.parseDouble(fields[5].getText()), Double.parseDouble(fields[6].getText()));
			} catch(Throwable err)
			{
				mc.player.sendMessage(new TextComponentString("Unable to parse translation!"));
			}
			
			try
			{
				t.rotation = new Vec3d(Double.parseDouble(fields[7].getText()), Double.parseDouble(fields[8].getText()), Double.parseDouble(fields[9].getText()));
			} catch(Throwable err)
			{
				mc.player.sendMessage(new TextComponentString("Unable to parse rotation!"));
			}
			
			HCNet.INSTANCE.sendToServer(new PacketSyncSyncableTile(t));
			
			mc.displayGuiScreen(null);
			mc.setIngameFocus();
		}
	}
}