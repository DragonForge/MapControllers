package com.zeitheron.mapcontrollers.net;

import com.zeitheron.hammercore.net.IPacket;
import com.zeitheron.hammercore.net.PacketContext;
import com.zeitheron.mapcontrollers.client.SoundManager;

import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.util.math.BlockPos;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;

public class PacketResumeSoundFaded implements IPacket
{
	static
	{
		IPacket.handle(PacketResumeSoundFaded.class, PacketResumeSoundFaded::new);
	}
	
	public BlockPos pos;
	public float fadeout;
	
	public PacketResumeSoundFaded()
	{
	}
	
	public PacketResumeSoundFaded(BlockPos pos, float fadeout)
	{
		this.pos = pos;
		this.fadeout = fadeout;
	}
	
	@Override
	@SideOnly(Side.CLIENT)
	public IPacket executeOnClient(PacketContext net)
	{
		SoundManager.resumeSound(pos, fadeout);
		return null;
	}
	
	@Override
	public void readFromNBT(NBTTagCompound nbt)
	{
		int[] p = nbt.getIntArray("p");
		pos = new BlockPos(p[0], p[1], p[2]);
		fadeout = nbt.getFloat("f");
	}
	
	@Override
	public void writeToNBT(NBTTagCompound nbt)
	{
		nbt.setIntArray("p", new int[] { pos.getX(), pos.getY(), pos.getZ() });
		nbt.setFloat("f", fadeout);
	}
}
