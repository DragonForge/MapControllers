package com.zeitheron.mapcontrollers.net;

import java.util.concurrent.atomic.AtomicInteger;

import org.apache.http.client.ClientProtocolException;

import com.zeitheron.hammercore.net.IPacket;
import com.zeitheron.hammercore.net.PacketContext;
import com.zeitheron.mapcontrollers.client.narrator.NarratorHelper;
import com.zeitheron.mapcontrollers.proxy.ClientProxy;

import net.minecraft.nbt.NBTTagCompound;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;

public class PacketNarrate implements IPacket
{
	static
	{
		IPacket.handle(PacketNarrate.class, PacketNarrate::new);
	}
	
	public String text;
	public boolean removeSomeChars;
	
	public PacketNarrate withText(String text, boolean removeSomeChars)
	{
		this.removeSomeChars = removeSomeChars;
		this.text = text;
		return this;
	}
	
	@Override
	public void writeToNBT(NBTTagCompound nbt)
	{
		nbt.setString("Text", text.trim());
	}
	
	@Override
	public void readFromNBT(NBTTagCompound nbt)
	{
		text = nbt.getString("Text");
	}
	
	@Override
	@SideOnly(Side.CLIENT)
	public IPacket executeOnClient(PacketContext net)
	{
		try
		{
			if(!text.isEmpty())
				NarratorHelper.sayable(text).onBegin(() -> ClientProxy.addSubtitle(text.trim())).onEnd(() -> ClientProxy.removeSubtitle(text)).submit();
		} catch(Throwable e)
		{
		}
		
		return null;
	}
}