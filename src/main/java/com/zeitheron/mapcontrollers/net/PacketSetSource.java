package com.zeitheron.mapcontrollers.net;

import com.zeitheron.hammercore.net.IPacket;
import com.zeitheron.hammercore.net.PacketContext;
import com.zeitheron.hammercore.tile.TileSyncable;
import com.zeitheron.hammercore.utils.WorldUtil;
import com.zeitheron.mapcontrollers.api.ISpeakerController;
import com.zeitheron.mapcontrollers.tile.TileNarratorController;

import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.math.BlockPos;

public class PacketSetSource implements IPacket
{
	static
	{
		IPacket.handle(PacketSetSource.class, PacketSetSource::new);
	}
	public BlockPos pos;
	public String url;
	
	public float rad;
	
	public PacketSetSource()
	{
	}
	
	public PacketSetSource(BlockPos pos, String url, float rad)
	{
		this.pos = pos;
		this.url = url;
		this.rad = rad;
	}
	
	@Override
	public IPacket executeOnServer(PacketContext net)
	{
		if(net.getSender() == null)
			return null;
		TileEntity tile = net.getSender().world.getTileEntity(pos);
		ISpeakerController c = WorldUtil.cast(tile, ISpeakerController.class);
		if(c != null)
		{
			c.setSound(url);
			c.setHearDist(rad);
		} else if(tile instanceof TileNarratorController)
		{
			TileNarratorController tnc = (TileNarratorController) tile;
			tnc.text = url;
			tnc.hearableDistance = rad;
		}
		if(tile instanceof TileSyncable)
			((TileSyncable) tile).sync();
		return null;
	}
	
	@Override
	public void readFromNBT(NBTTagCompound nbt)
	{
		int[] p = nbt.getIntArray("p");
		pos = new BlockPos(p[0], p[1], p[2]);
		url = nbt.getString("u");
		rad = nbt.getFloat("rad");
	}
	
	@Override
	public void writeToNBT(NBTTagCompound nbt)
	{
		nbt.setIntArray("p", new int[] { pos.getX(), pos.getY(), pos.getZ() });
		nbt.setString("u", url);
		nbt.setFloat("rad", rad);
	}
}