package com.zeitheron.mapcontrollers.net;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;

import com.zeitheron.hammercore.net.IPacket;
import com.zeitheron.hammercore.net.PacketContext;
import com.zeitheron.mapcontrollers.client.SoundManager;
import com.zeitheron.musiclayer.url.MusicCacher;

import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.util.math.BlockPos;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;

public class PacketStartSoundURL implements IPacket
{
	static
	{
		IPacket.handle(PacketStartSoundURL.class, PacketStartSoundURL::new);
	}
	public BlockPos pos;
	public String url;
	
	public float rad;
	
	public PacketStartSoundURL()
	{
	}
	
	public PacketStartSoundURL(BlockPos pos, String url, float rad)
	{
		this.pos = pos;
		this.url = url;
		this.rad = rad;
	}
	
	@Override
	@SideOnly(Side.CLIENT)
	public IPacket executeOnClient(PacketContext net)
	{
		new Thread(() ->
		{
			if(url.startsWith("http://") || url.startsWith("https://"))
				try
				{
					SoundManager.playSound(() -> new BufferedInputStream(MusicCacher.getMusicStream(url)), pos, rad);
				} catch(Throwable err)
				{
					err.printStackTrace();
				}
			else
				try
				{
					File ff;
					File f = new File("HammerCore", "Speakers" + File.separator + "sounds");
					if(!f.exists())
						f.mkdirs();
					if((ff = new File(f, url + ".ogg")).isFile())
						SoundManager.playSound(() ->
						{
							try
							{
								return new FileInputStream(ff);
							} catch(Throwable throwable)
							{
								return null;
							}
						}, pos, rad);
				} catch(Throwable err)
				{
					err.printStackTrace();
				}
		}).start();
		return null;
	}
	
	@Override
	public void readFromNBT(NBTTagCompound nbt)
	{
		int[] p = nbt.getIntArray("p");
		pos = new BlockPos(p[0], p[1], p[2]);
		url = nbt.getString("u");
		rad = nbt.getFloat("rad");
	}
	
	@Override
	public void writeToNBT(NBTTagCompound nbt)
	{
		nbt.setIntArray("p", new int[] { pos.getX(), pos.getY(), pos.getZ() });
		nbt.setString("u", url);
		nbt.setFloat("rad", rad);
	}
}
