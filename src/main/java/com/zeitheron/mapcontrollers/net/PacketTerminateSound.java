package com.zeitheron.mapcontrollers.net;

import com.zeitheron.hammercore.net.IPacket;
import com.zeitheron.hammercore.net.PacketContext;
import com.zeitheron.mapcontrollers.client.SoundManager;

import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.util.math.BlockPos;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;

public class PacketTerminateSound implements IPacket
{
	static
	{
		IPacket.handle(PacketTerminateSound.class, PacketTerminateSound::new);
	}
	
	public BlockPos pos;
	
	public PacketTerminateSound()
	{
	}
	
	public PacketTerminateSound(BlockPos pos)
	{
		this.pos = pos;
	}
	
	@Override
	@SideOnly(Side.CLIENT)
	public IPacket executeOnClient(PacketContext net)
	{
		SoundManager.stopSound(pos);
		return null;
	}
	
	@Override
	public void readFromNBT(NBTTagCompound nbt)
	{
		int[] p = nbt.getIntArray("p");
		pos = new BlockPos(p[0], p[1], p[2]);
	}
	
	@Override
	public void writeToNBT(NBTTagCompound nbt)
	{
		nbt.setIntArray("p", new int[] { pos.getX(), pos.getY(), pos.getZ() });
	}
}
