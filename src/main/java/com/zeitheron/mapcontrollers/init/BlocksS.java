package com.zeitheron.mapcontrollers.init;

import com.zeitheron.mapcontrollers.blocks.BlockDisplayController;
import com.zeitheron.mapcontrollers.blocks.BlockNarratorController;
import com.zeitheron.mapcontrollers.blocks.BlockSpeakerController;

public class BlocksS
{
	public static final BlockSpeakerController SPEAKER_CONTROLLER = new BlockSpeakerController();
	public static final BlockNarratorController NARRATOR_CONTROLLER = new BlockNarratorController();
	public static final BlockDisplayController DISPLAY_CONTROLLER = new BlockDisplayController();
}
