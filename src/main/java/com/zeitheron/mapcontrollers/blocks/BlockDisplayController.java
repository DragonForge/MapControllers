package com.zeitheron.mapcontrollers.blocks;

import java.util.Random;

import com.zeitheron.hammercore.api.ITileBlock;
import com.zeitheron.hammercore.proxy.ParticleProxy_Client;
import com.zeitheron.hammercore.utils.WorldLocation;
import com.zeitheron.mapcontrollers.MapControllers;
import com.zeitheron.mapcontrollers.client.fx.ParticleScreen;
import com.zeitheron.mapcontrollers.tile.TileDisplayController;

import net.minecraft.block.Block;
import net.minecraft.block.ITileEntityProvider;
import net.minecraft.block.SoundType;
import net.minecraft.block.material.Material;
import net.minecraft.block.state.IBlockState;
import net.minecraft.client.Minecraft;
import net.minecraft.client.entity.EntityPlayerSP;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.Item;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.EnumBlockRenderType;
import net.minecraft.util.EnumFacing;
import net.minecraft.util.EnumHand;
import net.minecraft.util.math.AxisAlignedBB;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.IBlockAccess;
import net.minecraft.world.World;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;

public class BlockDisplayController extends Block implements ITileEntityProvider, ITileBlock<TileDisplayController>
{
	public BlockDisplayController()
	{
		super(Material.IRON);
		setUnlocalizedName("display_screen");
		setSoundType(SoundType.METAL);
		setHardness(-1);
	}
	
	@Override
	public boolean isFullCube(IBlockState state)
	{
		return false;
	}
	
	@Override
	public boolean onBlockActivated(World worldIn, BlockPos pos, IBlockState state, EntityPlayer playerIn, EnumHand hand, EnumFacing facing, float hitX, float hitY, float hitZ)
	{
		MapControllers.proxy.openDisplayController(new WorldLocation(worldIn, pos));
		return true;
	}
	
	@Override
	public AxisAlignedBB getCollisionBoundingBox(IBlockState blockState, IBlockAccess worldIn, BlockPos pos)
	{
		return null;
	}
	
	@Override
	public TileEntity createNewTileEntity(World worldIn, int meta)
	{
		return new TileDisplayController();
	}
	
	@Override
	public Class<TileDisplayController> getTileClass()
	{
		return TileDisplayController.class;
	}
	
	@Override
	public boolean isOpaqueCube(IBlockState state)
	{
		return false;
	}
	
	@Override
	public boolean isFullBlock(IBlockState state)
	{
		return false;
	}
	
	@Override
	public boolean isPassable(IBlockAccess worldIn, BlockPos pos)
	{
		return true;
	}
	
	@Override
	public EnumBlockRenderType getRenderType(IBlockState state)
	{
		return EnumBlockRenderType.ENTITYBLOCK_ANIMATED;
	}
	
	@Override
	@SideOnly(Side.CLIENT)
	public void randomDisplayTick(IBlockState stateIn, World worldIn, BlockPos pos, Random rand)
	{
		EntityPlayerSP mp = Minecraft.getMinecraft().player;
		Item it = Item.getItemFromBlock(this);
		if(mp != null && mp.getHeldItemMainhand().getItem() == it || mp.getHeldItemOffhand().getItem() == it)
			ParticleProxy_Client.queueParticleSpawn(new ParticleScreen(worldIn, pos.getX() + .5, pos.getY() + .5, pos.getZ() + .5, it));
	}
}