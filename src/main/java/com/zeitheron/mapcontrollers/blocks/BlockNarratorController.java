package com.zeitheron.mapcontrollers.blocks;

import com.zeitheron.hammercore.internal.GuiManager;
import com.zeitheron.hammercore.internal.blocks.base.BlockDeviceHC;
import com.zeitheron.hammercore.internal.blocks.base.IBlockEnableable;
import com.zeitheron.hammercore.tile.TileSyncable;
import com.zeitheron.hammercore.utils.WorldUtil;
import com.zeitheron.mapcontrollers.tile.TileNarratorController;

import net.minecraft.block.Block;
import net.minecraft.block.SoundType;
import net.minecraft.block.material.Material;
import net.minecraft.block.state.IBlockState;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.EnumFacing;
import net.minecraft.util.EnumHand;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;

public class BlockNarratorController extends BlockDeviceHC<TileNarratorController> implements IBlockEnableable
{
	public BlockNarratorController()
	{
		super(Material.IRON, TileNarratorController.class, "narrator_controller");
		setSoundType(SoundType.METAL);
		setHardness(4F);
		setHarvestLevel("pickaxe", 1);
	}
	
	@Override
	public void neighborChanged(IBlockState state, World worldIn, BlockPos pos, Block blockIn, BlockPos frompos)
	{
		boolean powered = worldIn.isBlockIndirectlyGettingPowered(pos) > 0;
		if(state.getValue(IBlockEnableable.ENABLED) != powered)
		{
			TileEntity te = worldIn.getTileEntity(pos);
			worldIn.setBlockState(pos, state.withProperty(IBlockEnableable.ENABLED, powered));
			if(te != null)
			{
				if(te instanceof TileNarratorController && powered)
					((TileNarratorController) te).say();
				te.validate();
				worldIn.setTileEntity(pos, te);
			}
		}
	}
	
	@Override
	public boolean onBlockActivated(World worldIn, BlockPos pos, IBlockState state, EntityPlayer playerIn, EnumHand hand, EnumFacing facing, float hitX, float hitY, float hitZ)
	{
		GuiManager.openGui(playerIn, WorldUtil.cast(worldIn.getTileEntity(pos), TileSyncable.class));
		return true;
	}
}