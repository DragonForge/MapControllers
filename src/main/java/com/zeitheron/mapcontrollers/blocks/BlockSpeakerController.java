package com.zeitheron.mapcontrollers.blocks;

import com.zeitheron.hammercore.internal.GuiManager;
import com.zeitheron.hammercore.internal.blocks.base.BlockDeviceHC;
import com.zeitheron.hammercore.net.HCNet;
import com.zeitheron.hammercore.tile.TileSyncable;
import com.zeitheron.hammercore.utils.WorldUtil;
import com.zeitheron.mapcontrollers.net.PacketTerminateSound;
import com.zeitheron.mapcontrollers.tile.TileSpeakerController;

import net.minecraft.block.SoundType;
import net.minecraft.block.material.Material;
import net.minecraft.block.state.IBlockState;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.util.EnumFacing;
import net.minecraft.util.EnumHand;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;

public class BlockSpeakerController extends BlockDeviceHC<TileSpeakerController>
{
	public BlockSpeakerController()
	{
		super(Material.WOOD, TileSpeakerController.class, "speaker_controller");
		setHarvestLevel("axe", 0);
		setSoundType(SoundType.WOOD);
		setHardness(2.0f);
	}
	
	@Override
	public void breakBlock(World worldIn, BlockPos pos, IBlockState state)
	{
		if(!worldIn.isRemote)
			HCNet.INSTANCE.sendToDimension(new PacketTerminateSound(pos), worldIn.provider.getDimension());
		super.breakBlock(worldIn, pos, state);
	}
	
	@Override
	public boolean onBlockActivated(World worldIn, BlockPos pos, IBlockState state, EntityPlayer playerIn, EnumHand hand, EnumFacing facing, float hitX, float hitY, float hitZ)
	{
		GuiManager.openGui(playerIn, WorldUtil.cast(worldIn.getTileEntity(pos), TileSyncable.class));
		return true;
	}
}