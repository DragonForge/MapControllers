package com.zeitheron.mapcontrollers.tile;

import com.zeitheron.hammercore.client.gui.impl.container.ContainerEmpty;
import com.zeitheron.hammercore.net.HCNet;
import com.zeitheron.hammercore.tile.TileSyncable;
import com.zeitheron.mapcontrollers.client.gui.GuiNarratorController;
import com.zeitheron.mapcontrollers.net.PacketNarrate;

import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraftforge.fml.common.network.NetworkRegistry.TargetPoint;

public class TileNarratorController extends TileSyncable
{
	public String text = "";
	public float hearableDistance = 256;
	public boolean removeSomeChars;
	
	public void say()
	{
		if(!world.isRemote && !text.isEmpty())
			HCNet.INSTANCE.sendToAllAround(new PacketNarrate().withText(text, removeSomeChars), new TargetPoint(world.provider.getDimension(), pos.getX() + .5, pos.getY() + .5, pos.getZ() + .5, hearableDistance + 1));
	}
	
	@Override
	public void writeNBT(NBTTagCompound nbt)
	{
		nbt.setString("Text", text);
		nbt.setBoolean("EscapeChars", removeSomeChars);
		nbt.setFloat("Radius", hearableDistance);
	}
	
	@Override
	public void readNBT(NBTTagCompound nbt)
	{
		text = nbt.getString("Text");
		removeSomeChars = nbt.getBoolean("EscapeChars");
		hearableDistance = nbt.getFloat("Radius");
	}
	
	@Override
	public boolean hasGui()
	{
		return true;
	}
	
	@Override
	public Object getClientGuiElement(EntityPlayer player)
	{
		return new GuiNarratorController(this);
	}
	
	@Override
	public Object getServerGuiElement(EntityPlayer player)
	{
		return new ContainerEmpty();
	}
}