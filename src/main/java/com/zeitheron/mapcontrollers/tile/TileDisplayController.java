package com.zeitheron.mapcontrollers.tile;

import com.zeitheron.hammercore.tile.TileSyncableTickable;
import com.zeitheron.mapcontrollers.client.renderable.IBindableImage;

import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.util.math.AxisAlignedBB;
import net.minecraft.util.math.Vec3d;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;

public class TileDisplayController extends TileSyncableTickable
{
	public String image = "";
	
	public IBindableImage bindable;
	
	@SideOnly(Side.CLIENT)
	public IBindableImage getBindable()
	{
		if(bindable == null || !bindable.isSameURL(image))
			bindable = IBindableImage.probe(image);
		return bindable;
	}
	
	private static final Vec3d ONE = new Vec3d(1, 1, 1);
	public Vec3d rotation, scale, translation;
	
	private boolean synced = false;
	
	@Override
	public void tick()
	{
		if(!synced)
		{
			synced = true;
			sendChangesToNearby();
		}
	}
	
	@Override
	public void writeNBT(NBTTagCompound nbt)
	{
		nbt.setString("URL", image);
		saveVec3d(nbt, rotation, "Rotation");
		saveVec3d(nbt, scale, "Scale");
		saveVec3d(nbt, translation, "Translation");
	}
	
	@Override
	public void readNBT(NBTTagCompound nbt)
	{
		image = nbt.getString("URL");
		rotation = loadVec3d(nbt, "Rotation");
		scale = loadVec3d(nbt, "Scale");
		translation = loadVec3d(nbt, "Translation");
		synced = false;
	}
	
	public Vec3d getRotation()
	{
		if(rotation == null)
			return Vec3d.ZERO;
		return rotation;
	}
	
	public Vec3d getScale()
	{
		if(scale == null || (scale.x == scale.y && scale.y == scale.z && scale.x == 0))
			return ONE;
		return scale;
	}
	
	public Vec3d getTranslation()
	{
		if(translation == null)
			return Vec3d.ZERO;
		return translation;
	}
	
	public void setImage(String image)
	{
		this.image = image;
		sync();
	}
	
	@Override
	public AxisAlignedBB getRenderBoundingBox()
	{
		return INFINITE_EXTENT_AABB;
	}
	
	@Override
	@SideOnly(Side.CLIENT)
	public double getMaxRenderDistanceSquared()
	{
		return Double.POSITIVE_INFINITY;
	}
	
	public static void saveVec3d(NBTTagCompound nbt, Vec3d vec, String name)
	{
		if(vec == null)
			vec = Vec3d.ZERO;
		nbt.setDouble(name + "X", vec.x);
		nbt.setDouble(name + "Y", vec.y);
		nbt.setDouble(name + "Z", vec.z);
	}
	
	public static Vec3d loadVec3d(NBTTagCompound nbt, String name)
	{
		return new Vec3d(nbt.getDouble(name + "X"), nbt.getDouble(name + "Y"), nbt.getDouble(name + "Z"));
	}
}