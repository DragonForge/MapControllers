package com.zeitheron.mapcontrollers.tile;

import java.util.ArrayList;
import java.util.List;

import com.zeitheron.hammercore.client.gui.impl.container.ContainerEmpty;
import com.zeitheron.hammercore.net.HCNet;
import com.zeitheron.hammercore.tile.TileSyncableTickable;
import com.zeitheron.hammercore.utils.WorldLocation;
import com.zeitheron.hammercore.utils.WorldUtil;
import com.zeitheron.mapcontrollers.api.ISpeakerController;
import com.zeitheron.mapcontrollers.api.ISpeakerPart;
import com.zeitheron.mapcontrollers.client.gui.GuiSpeakerController;
import com.zeitheron.mapcontrollers.net.PacketPauseSound;
import com.zeitheron.mapcontrollers.net.PacketPauseSoundFaded;
import com.zeitheron.mapcontrollers.net.PacketResumeSound;
import com.zeitheron.mapcontrollers.net.PacketResumeSoundFaded;
import com.zeitheron.mapcontrollers.net.PacketStartSoundURL;
import com.zeitheron.mapcontrollers.net.PacketTerminateSound;

import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.util.EnumFacing;

public class TileSpeakerController extends TileSyncableTickable implements ISpeakerController
{
	public String sound = "";
	public float rad;
	public List<ISpeakerPart> parts = new ArrayList<>();
	public boolean started;
	public boolean paused;
	
	@Override
	public boolean attach(ISpeakerPart part)
	{
		if(parts.contains(part))
			return false;
		for(ISpeakerPart p : parts)
		{
			if(!p.loc().getPos().equals(part.loc().getPos()))
				continue;
			return false;
		}
		parts.add(part);
		part.setController(this);
		return true;
	}
	
	@Override
	public boolean detach(ISpeakerPart part)
	{
		return parts.remove(part);
	}
	
	@Override
	public Object getClientGuiElement(EntityPlayer player)
	{
		return new GuiSpeakerController(this);
	}
	
	@Override
	public Object getServerGuiElement(EntityPlayer player)
	{
		return new ContainerEmpty();
	}
	
	@Override
	public boolean hasGui()
	{
		return true;
	}
	
	@Override
	public boolean isExpandedWith(String expansion)
	{
		for(ISpeakerPart part : parts)
		{
			if(!part.provides(expansion))
				continue;
			return true;
		}
		return false;
	}
	
	@Override
	public WorldLocation loc()
	{
		return getLocation();
	}
	
	@Override
	public void pause()
	{
		paused = true;
		HCNet.INSTANCE.sendToAllAround(new PacketPauseSound(pos), getSyncPoint((int) Math.ceil(rad)));
	}
	
	@Override
	public void readNBT(NBTTagCompound nbt)
	{
		sound = nbt.getString("Sound");
		rad = nbt.getFloat("Volume");
	}
	
	@Override
	public void resume()
	{
		paused = false;
		HCNet.INSTANCE.sendToAllAround(new PacketResumeSound(pos), getSyncPoint((int) Math.ceil(rad)));
	}
	
	@Override
	public void setHearDist(float rad)
	{
		this.rad = rad;
	}
	
	@Override
	public boolean setSound(String location)
	{
		sound = location;
		if(started)
			stop();
		return true;
	}
	
	@Override
	public void start()
	{
		if(started)
			stop();
		started = true;
		paused = false;
		HCNet.INSTANCE.sendToAllAround(new PacketStartSoundURL(pos, sound, rad), getSyncPoint((int) Math.ceil(rad)));
	}
	
	@Override
	public void stop()
	{
		started = false;
		paused = false;
		HCNet.INSTANCE.sendToAllAround(new PacketTerminateSound(pos), getSyncPoint((int) Math.ceil(rad)));
	}
	
	@Override
	public void tick()
	{
		if(world.isRemote)
			return;
		for(EnumFacing f : EnumFacing.VALUES)
		{
			ISpeakerPart part = WorldUtil.cast((Object) getLocation().offset(f), ISpeakerPart.class);
			if(part == null)
				continue;
			attach(part);
		}
	}
	
	@Override
	public void writeNBT(NBTTagCompound nbt)
	{
		nbt.setString("Sound", sound);
		nbt.setFloat("Volume", rad);
	}

	@Override
	public void pause(float fadeout)
	{
		paused = true;
		HCNet.INSTANCE.sendToAllAround(new PacketPauseSoundFaded(pos, fadeout), getSyncPoint((int) Math.ceil(rad)));
	}

	@Override
	public void resume(float fadein)
	{
		paused = false;
		HCNet.INSTANCE.sendToAllAround(new PacketResumeSoundFaded(pos, fadein), getSyncPoint((int) Math.ceil(rad)));
	}
}
