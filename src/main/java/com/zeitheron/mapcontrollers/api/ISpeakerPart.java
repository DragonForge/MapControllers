package com.zeitheron.mapcontrollers.api;

import javax.annotation.Nullable;

import com.zeitheron.hammercore.utils.WorldLocation;

public interface ISpeakerPart
{
	public ISpeakerController getController();
	
	public WorldLocation loc();
	
	@Nullable
	public boolean provides(String var1);
	
	public boolean setController(ISpeakerController var1);
}