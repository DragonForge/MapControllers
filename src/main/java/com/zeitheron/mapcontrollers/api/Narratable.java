package com.zeitheron.mapcontrollers.api;

import java.util.ArrayList;
import java.util.List;
import java.util.Queue;
import java.util.function.Consumer;

import com.google.common.collect.Queues;
import com.zeitheron.hammercore.lib.zlib.utils.Threading;

public class Narratable implements java.io.Serializable
{
	protected static final Queue<Narratable> msgs = Queues.newConcurrentLinkedQueue();
	
	static
	{
		Threading.createAndStart("MapControllerNarrator", () ->
		{
			while(true)
			{
				if(msgs.peek() != null)
				{
					Narratable n = msgs.poll();
					n.blockingNarrate.accept(n);
				}
				
				try
				{
					Thread.sleep(100);
				} catch(final InterruptedException e)
				{
					e.printStackTrace();
				}
			}
		});
	}
	
	private static final long serialVersionUID = 4654671469794556979L;
	
	private volatile boolean value;
	
	public final List<Consumer<Narratable>> listeners = new ArrayList<>();
	
	private final Consumer<Narratable> blockingNarrate;
	
	public Narratable(boolean initialValue, Consumer<Narratable> blockingNarrate)
	{
		this.blockingNarrate = blockingNarrate;
		value = initialValue;
	}
	
	public Narratable submit()
	{
		msgs.add(this);
		return this;
	}
	
	public Narratable onEnd(Runnable run)
	{
		listeners.add(v ->
		{
			if(v.get())
				run.run();
		});
		return this;
	}
	
	public Narratable onBegin(Runnable run)
	{
		listeners.add(v ->
		{
			if(!v.get())
				run.run();
		});
		return this;
	}
	
	public final boolean get()
	{
		return value;
	}
	
	public final void set(boolean newValue)
	{
		value = newValue;
		listeners.forEach(c -> c.accept(this));
	}
	
	public final boolean getAndSet(boolean newValue)
	{
		boolean prev = get();
		set(newValue);
		return prev;
	}
	
	@Override
	public String toString()
	{
		return Boolean.toString(get());
	}
}