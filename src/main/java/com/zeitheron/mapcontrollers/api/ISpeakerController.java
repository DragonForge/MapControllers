package com.zeitheron.mapcontrollers.api;

import com.zeitheron.hammercore.utils.WorldLocation;

public interface ISpeakerController
{
	public boolean attach(ISpeakerPart var1);
	
	public boolean detach(ISpeakerPart var1);
	
	public boolean isExpandedWith(String var1);
	
	public WorldLocation loc();
	
	public void pause();
	
	public void pause(float fadeout);
	
	public void resume();
	
	public void resume(float fadein);
	
	public void setHearDist(float var1);
	
	public boolean setSound(String var1);
	
	public void start();
	
	public void stop();
}