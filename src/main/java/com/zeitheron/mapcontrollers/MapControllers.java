package com.zeitheron.mapcontrollers;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.zeitheron.hammercore.api.GameRules;
import com.zeitheron.hammercore.api.GameRules.GameRuleEntry;
import com.zeitheron.hammercore.api.GameRules.ValueType;
import com.zeitheron.hammercore.internal.SimpleRegistration;
import com.zeitheron.hammercore.internal.variables.VariableManager;
import com.zeitheron.hammercore.internal.variables.types.VariableInt;
import com.zeitheron.mapcontrollers.commands.CommandNarrate;
import com.zeitheron.mapcontrollers.commands.CommandSpeakers;
import com.zeitheron.mapcontrollers.init.BlocksS;
import com.zeitheron.mapcontrollers.proxy.CommonProxy;

import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.server.MinecraftServer;
import net.minecraft.world.WorldServer;
import net.minecraftforge.common.MinecraftForge;
import net.minecraftforge.fml.common.FMLCommonHandler;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.common.SidedProxy;
import net.minecraftforge.fml.common.event.FMLConstructionEvent;
import net.minecraftforge.fml.common.event.FMLInitializationEvent;
import net.minecraftforge.fml.common.event.FMLPreInitializationEvent;
import net.minecraftforge.fml.common.event.FMLServerStartingEvent;
import net.minecraftforge.fml.common.eventhandler.SubscribeEvent;
import net.minecraftforge.fml.common.gameevent.TickEvent.Phase;
import net.minecraftforge.fml.common.gameevent.TickEvent.ServerTickEvent;

@Mod(modid = MapControllers.MOD_ID, dependencies = "required-after:hammercore;required-after:musiclayer", version = "@VERSION@", name = "Map Controllers")
public class MapControllers
{
	public static final String MOD_ID = "mapcontrollers";
	
	@Mod.Instance
	public static MapControllers instance;
	
	@SidedProxy(clientSide = "com.zeitheron.mapcontrollers.proxy.ClientProxy", serverSide = "com.zeitheron.mapcontrollers.proxy.CommonProxy")
	public static CommonProxy proxy;
	
	public static Logger LOG = LogManager.getLogger(MapControllers.MOD_ID);
	
	public static final VariableInt NARRATABLE_SUBTITLES = new VariableInt(MOD_ID + ":1");
	
	@Mod.EventHandler
	public void construct(FMLConstructionEvent e)
	{
		proxy.construct();
	}
	
	@Mod.EventHandler
	public void init(FMLInitializationEvent e)
	{
		proxy.init();
		VariableManager.registerVariable(NARRATABLE_SUBTITLES);
		MinecraftForge.EVENT_BUS.register(proxy);
		MinecraftForge.EVENT_BUS.register(this);
	}
	
	@Mod.EventHandler
	public void preInit(FMLPreInitializationEvent e)
	{
		GameRules.registerGameRule(new GameRuleEntry(MOD_ID + "_narrator_subtitles", "true", "gamerules." + MOD_ID + "_subtitles", ValueType.BOOLEAN_VALUE));
		SimpleRegistration.registerFieldBlocksFrom(BlocksS.class, MOD_ID, CreativeTabs.REDSTONE);
	}
	
	@Mod.EventHandler
	public void serverStarting(FMLServerStartingEvent e)
	{
		e.registerServerCommand(new CommandSpeakers());
		e.registerServerCommand(new CommandNarrate());
	}
	
	@SubscribeEvent
	public void serverTick(ServerTickEvent e)
	{
		if(e.phase == Phase.END)
		{
			MinecraftServer mcs = FMLCommonHandler.instance().getMinecraftServerInstance();
			if(mcs != null && mcs.worlds.length > 0)
			{
				WorldServer overworld = mcs.worlds[0];
				if(overworld != null)
					NARRATABLE_SUBTITLES.set(overworld.getGameRules().getBoolean(MOD_ID + "_narrator_subtitles") ? 1 : 0);
			}
		}
	}
}