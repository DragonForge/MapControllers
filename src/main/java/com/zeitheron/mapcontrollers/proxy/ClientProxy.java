package com.zeitheron.mapcontrollers.proxy;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.lwjgl.opengl.GL11;

import com.zeitheron.hammercore.api.IProcess;
import com.zeitheron.hammercore.client.utils.RenderUtil;
import com.zeitheron.hammercore.utils.WorldLocation;
import com.zeitheron.hammercore.utils.color.ColorHelper;
import com.zeitheron.mapcontrollers.MapControllers;
import com.zeitheron.mapcontrollers.client.FadeableString;
import com.zeitheron.mapcontrollers.client.SoundManager;
import com.zeitheron.mapcontrollers.client.gui.GuiDisplayController;
import com.zeitheron.mapcontrollers.client.renderable.ImagePuller;
import com.zeitheron.mapcontrollers.tile.TileDisplayController;

import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.FontRenderer;
import net.minecraft.client.gui.ScaledResolution;
import net.minecraft.client.renderer.GlStateManager;
import net.minecraftforge.client.event.RenderGameOverlayEvent;
import net.minecraftforge.client.event.RenderGameOverlayEvent.ElementType;
import net.minecraftforge.common.MinecraftForge;
import net.minecraftforge.fml.common.eventhandler.EventPriority;
import net.minecraftforge.fml.common.eventhandler.SubscribeEvent;
import net.minecraftforge.fml.common.network.FMLNetworkEvent;

public class ClientProxy extends CommonProxy
{
	public static List<FadeableString> renderSubtitles = new ArrayList<>();
	
	public static FadeableString addSubtitle(String text)
	{
		FadeableString fs;
		renderSubtitles.add(fs = new FadeableString(text));
		fs.fadeIn = 0;
		new IProcess()
		{
			public int ticksExisted;
			
			@Override
			public void update()
			{
				++ticksExisted;
				fs.fadeIn = Math.min(1, fs.fadeIn + 1F / fs.expectedFadeInTicks);
			}
			
			@Override
			public boolean isAlive()
			{
				return fs.fadeIn < 1F;
			}
		}.start();
		
		return fs;
	}
	
	public static void removeSubtitle(String code)
	{
		Optional<FadeableString> s = renderSubtitles.stream().filter(f -> f.toString().equals(code)).findFirst();
		if(s.isPresent())
		{
			new IProcess()
			{
				public int ticksExisted;
				
				@Override
				public void update()
				{
					++ticksExisted;
					FadeableString fs = s.get();
					fs.fadeOut = Math.min(1, fs.fadeOut + 1F / fs.expectedFadeOutTicks);
				}
				
				@Override
				public boolean isAlive()
				{
					return s.get().fadeOut < 1F;
				}
			}.start();
		}
	}
	
	@Override
	public void construct()
	{
	}
	
	@Override
	public void init()
	{
		MinecraftForge.EVENT_BUS.register(new SoundManager());
	}
	
	@Override
	public void openDisplayController(WorldLocation loc)
	{
		if(loc == null)
			return;
		TileDisplayController t = loc.getTileOfType(TileDisplayController.class);
		if(loc.getWorld().isRemote && t != null)
			Minecraft.getMinecraft().displayGuiScreen(new GuiDisplayController(t));
	}
	
	@SubscribeEvent
	public void clientLogoff(FMLNetworkEvent.ClientDisconnectionFromServerEvent e)
	{
		ImagePuller.cleanupAll();
	}
	
	@SubscribeEvent(priority = EventPriority.LOWEST)
	public void renderSubtitles(RenderGameOverlayEvent e)
	{
		if(e.getType() != ElementType.TEXT)
			return;
		
		FontRenderer fr = Minecraft.getMinecraft().fontRenderer;
		
		boolean render = MapControllers.NARRATABLE_SUBTITLES.getInt() == 1;
		if(!render)
			renderSubtitles.clear();
		else if(!renderSubtitles.isEmpty())
		{
			renderSubtitles.removeIf(f -> f.fadeOut >= 1F);
			ScaledResolution sr = new ScaledResolution(Minecraft.getMinecraft());
			int width = sr.getScaledWidth() - 16;
			int height = sr.getScaledHeight();
			
			// renderSubtitles.clear();
			// renderSubtitles.add("Test line lololo test test lol kek cheburek
			// wtf
			// howtobasic what am I writing over here anyway?! >.< This is a
			// garbage line lol it's too long so it wraps into multiple lines");
			
			int totalHeight = 0;
			for(int i = 0; i < renderSubtitles.size(); ++i)
				totalHeight += fr.getWordWrappedHeight(renderSubtitles.get(i).toString(), width);
			
			GL11.glPushMatrix();
			GlStateManager.enableBlend();
			int lx = width, mxw = 0;
			float mxp = 0;
			GL11.glTranslated(0, 0, 1);
			int y = height - totalHeight - 36;
			for(FadeableString ln_ : renderSubtitles)
				for(String ln : fr.listFormattedStringToWidth(ln_.toString(), width))
				{
					float afo = Math.min(1, ln_.fadeOut + e.getPartialTicks() * (1F / ln_.expectedFadeOutTicks));
					float afi = Math.min(1, ln_.fadeIn + e.getPartialTicks() * (1F / ln_.expectedFadeInTicks));
					
					float progress = afo > 0F ? 1 - afo : afi;
					int w = fr.getStringWidth(ln);
					int x = 8 + (width - w) / 2;
					fr.drawString(ln, x, y - ((afi - 1) * fr.FONT_HEIGHT / 2F) - (afo * fr.FONT_HEIGHT / 2F), ((int) (progress * 255)) << 24 | 0xFFFFFF, true);
					lx = Math.min(lx, x);
					mxw = Math.max(mxw, w);
					mxp = Math.max(mxp, progress);
					y += fr.FONT_HEIGHT;
				}
			GL11.glTranslated(0, 0, -.01);
			GlStateManager.disableTexture2D();
			RenderUtil.drawColoredModalRect(lx - 8, height - totalHeight - 36 - 6, mxw + 16, totalHeight + 12, (int) (mxp * 100F) << 24);
			GlStateManager.enableTexture2D();
			GL11.glPopMatrix();
		}
	}
}